package me.lpk.backdoor.util;

import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class ChatUtil {
	public static String blue(Object o) {
		return ChatColor.BLUE + o.toString();
	}

	public static String gray(Object o) {
		return ChatColor.GRAY + o.toString();
	}

	/**
	 * stolen from Apache Ant's
	 * org.apache.tools.ant.types.CommandLine.translateCommandLine(String) c:
	 */
	public static String[] translateCommandline(String toProcess) {
		if (toProcess == null || toProcess.length() == 0) {
			return new String[0];
		}

		final int normal = 0;
		final int inQuote = 1;
		final int inDoubleQuote = 2;
		int state = normal;
		final StringTokenizer tok = new StringTokenizer(toProcess, "\"\' ",
				true);
		final ArrayList<String> result = new ArrayList<>();
		final StringBuilder current = new StringBuilder();
		boolean lastTokenHasBeenQuoted = false;

		while (tok.hasMoreTokens()) {
			String nextTok = tok.nextToken();
			switch (state) {
				case inQuote:
					if ("\'".equals(nextTok)) {
						lastTokenHasBeenQuoted = true;
						state = normal;
					} else {
						current.append(nextTok);
					}
					break;
				case inDoubleQuote:
					if ("\"".equals(nextTok)) {
						lastTokenHasBeenQuoted = true;
						state = normal;
					} else {
						current.append(nextTok);
					}
					break;
				default:
					if ("\'".equals(nextTok)) {
						state = inQuote;
					} else if ("\"".equals(nextTok)) {
						state = inDoubleQuote;
					} else if (" ".equals(nextTok)) {
						if (lastTokenHasBeenQuoted || current.length() != 0) {
							result.add(current.toString());
							current.setLength(0);
						}
					} else {
						current.append(nextTok);
					}
					lastTokenHasBeenQuoted = false;
					break;
			}
		}
		if (lastTokenHasBeenQuoted || current.length() != 0) {
			result.add(current.toString());
		}
		return result.toArray(new String[result.size()]);
	}
}
