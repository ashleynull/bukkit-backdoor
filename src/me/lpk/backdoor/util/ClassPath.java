package me.lpk.backdoor.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Written originally by Andy, modifications have been made from the original.
 *
 * @author <a href="https://stackoverflow.com/a/19554704/8071915">Andy</a>
 */
public class ClassPath {
	public static class ClassInfo {
		public final String className;
		public final String resourceName;
		public final byte[] bytecode;

		public ClassInfo(String name, byte[] bytecode) {
			this.className = name;
			this.resourceName = name.replace(".", "/") + ".class";
			this.bytecode = bytecode;
		}
	}

	public static Map<String, ClassInfo> getAllClasses() {
		return getAllClasses(false);
	}

	public static Map<String, ClassInfo> getAllClasses(boolean includeJavaPath) {
		Map<String, ClassInfo> map = new HashMap<>();
		initSearch(map, includeJavaPath, new Backport.Predicate<String>() {
			@Override
			public boolean test(String arg) {
				return true;
			}
		});
		return map;
	}

	public static Map<String, ClassInfo> getClasses(String package_) {
		return getClasses(package_, false, false);
	}

	public static Map<String, ClassInfo> getClasses(String package_, boolean recursive) {
		return getClasses(package_, recursive, false);
	}

	public static Map<String, ClassInfo> getClasses(final String package_, final boolean recursive, final boolean includeJavaPath) {
		Map<String, ClassInfo> map = new HashMap<>();
		initSearch(map, includeJavaPath, new Backport.Predicate<String>() {
			@Override
			public boolean test(String name) {
				return validName(recursive, package_, name);
			}

		});
		return map;
	}

	private static void initSearch(Map<String, ClassInfo> map, boolean includeJavaPath, Backport.Predicate<String> predicate) {
		String classpath = System.getProperty("java.class.path");
		String[] paths = classpath.split(System.getProperty("path.separator"));

		String javaHome = System.getProperty("java.home");
		File file = new File(javaHome + File.separator + "lib");
		if (includeJavaPath && file.exists()) {
			search(file, file, map, predicate);
		}

		for (String path : paths) {
			file = new File(path);
			if (file.exists()) {
				search(file, file, map, predicate);
			}
		}
	}

	private static boolean search(File root, File file, Map<String, ClassInfo> map, Backport.Predicate<String> filter) {
		if (file.isDirectory()) {
			for (File child : file.listFiles()) {
				if (!search(root, child, map, filter)) {
					return false;
				}
			}
		} else {
			try {
				if (file.getName().toLowerCase().endsWith(".jar")) {
					JarFile jar = null;
					try {
						jar = new JarFile(file);
					} catch (Exception ex) {
					}
					if (jar != null) {

						Enumeration<JarEntry> entries = jar.entries();
						while (entries.hasMoreElements()) {
							JarEntry entry = entries.nextElement();
							String name = entry.getName();
							int extIndex = name.lastIndexOf(".class");
							if (extIndex > 0) {
								String className = name.substring(0, extIndex).replace("/", ".");
								if (filter.test(className)) {
									ClassInfo info = new ClassInfo(className, IOUtil.toByteArray(jar.getInputStream(entry)));
									map.put(info.resourceName, info);
								}
							}
						}
					}
				} else if (file.getName().toLowerCase().endsWith(".class")) {
					String className = createClassName(root, file);
					if (filter.test(className)) {
						ClassInfo info = new ClassInfo(className, IOUtil.toByteArray(new FileInputStream(file)));
						map.put(info.resourceName, info);
					}
				}
			} catch (Exception e) {
			}
		}

		return true;
	}

	private static String createClassName(File root, File file) {
		StringBuffer sb = new StringBuffer();
		String fileName = file.getName();
		sb.append(fileName.substring(0, fileName.lastIndexOf(".class")));
		file = file.getParentFile();
		while (file != null && !file.equals(root)) {
			sb.insert(0, '.').insert(0, file.getName());
			file = file.getParentFile();
		}
		return sb.toString();
	}

	private static boolean validName(boolean recursive, String pkg, String name) {
		int i = name.lastIndexOf(".");
		String classPackage = (i == -1) ? "" : name.substring(0, i);
		return ((recursive && classPackage.startsWith(pkg)) || (!recursive && pkg.equals(classPackage)));
	}
}
