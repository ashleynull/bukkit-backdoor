package me.lpk.backdoor.util;

public class Convert {

	/**
	 * Parse an integer from a string.
	 *
	 * @param arg
	 * @return
	 */
	public static int toInt(String arg) {
		try {
			return Integer.parseInt(arg);
		} catch (Exception e) {
			return 0;
		}
	}

	/**
	 * Parse a boolean from a string.
	 *
	 * @param arg
	 * @return
	 */
	public static boolean toBool(String arg) {
		try {
			return Boolean.parseBoolean(arg);
		} catch (Exception e) {
			return false;
		}
	}
}
