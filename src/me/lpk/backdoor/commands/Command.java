package me.lpk.backdoor.commands;

import me.lpk.backdoor.util.StringArray;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * Command used by the file navigator.
 */
public abstract class Command {
	/**
	 * Command name.
	 */
	private final String[] names;
	/**
	 * Command description.
	 */
	private final String desc;

	public Command(String[] name) {
		this(name, "");
	}

	public Command(String[] names, String desc) {
		this.names = names;
		this.desc = desc;
	}

	/**
	 * Called when the command is fired.
	 *
	 * @param player The player firing the command.
	 * @param args   Argument. May be null.
	 */
	public abstract void onAction(Player player, StringArray args);

	/**
	 * Return the command's name.
	 *
	 * @return
	 */
	public String[] getNames() {
		return names;
	}

	/**
	 * Return the command's description.
	 *
	 * @return
	 */
	public String getDesc() {
		return desc;
	}


	/**
	 * Print a message to the given player.
	 *
	 * @param player
	 * @param msg
	 */
	protected void print(Player player, String msg) {
		player.sendMessage(msg);
	}

	/**
	 * Print a message to the given player in red text.
	 *
	 * @param player
	 * @param msg
	 */
	protected void error(Player player, String msg) {
		player.sendMessage(ChatColor.DARK_RED + msg);
	}
}
